#pragma once

#define STOP    false
#define START   true


void leds_init(void);
void leds_update(uint8_t value);
void leds_all_keys_off(void);
void leds_all_keys_on(uint8_t red, uint8_t green, uint8_t blue);
void leds_single_key_on_random(uint8_t x, uint8_t y);
void leds_key_row_on_random(uint8_t x, bool newcolour, bool configure);
void leds_key_column_on_random(uint8_t y, bool newcolour, bool configure);

//Having fun
void leds_update_control_color(void);
void leds_configure_lock_update(bool state);
void start_fun_with_cafeleds(void);
void fun_with_cafeleds(void);
void stop_fun_with_cafeleds(void);
